# Cápsula de Maxxcan


## Contenido

=> about.gmi  👨 Sobre mí y el sentido de esta cápsula (y la vida)

=> /gemlog 📝 🇪🇸   Gemlog en español

=> /gemlog-english 📝 🇺🇸   Gemlog en inglés

=> /videojuegos  Videojuegos

=> /retroinformatica Retroinformática

=> diario.gmi 📓  Anotaciones y pensamientos breves del día a día

=> /informatica 💻 Mi informática

=> /relatos ✍ Relatos de creación propia



## Sobre Gemini

=> /gemini/introduccion.gmi ♊ 📝 Introducción rápida a qué es Gemini

=> /gemini/marcadores.gmi 🕸 Enlaces interesantes

=> /gemini/capsulas.gmi 💊 Cápsulas amigas
