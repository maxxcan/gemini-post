 En mi caso, que me ha tocado llevar el sonido en alguna retransmisión, siempre he comentado que, si en lugar de la
 mezcla de sonido de la banda de música, aplausos, bravos, olessss y demás… el sonido fuera el que capta el Sennheiser
 816 (micrófono que capta a gran distancia y buena calidad) a pie de ruedo, donde se escucha perfectamente el sonido
 de la banderillas al entrar en la piel, los mugidos de dolor que da el animal a cada tortura a la que se somete… y
 además lo acompañáramos de primeros planos de las heridas que lleva, de los coágulos como la palma de una mano, de la
 sangre que le brota acompasada al latir del corazón o la mirada que pone en animal antes de que le den la estocada
 final, creo que el 90% apagaría el televisor al presenciar semejante carnicería a ritmo de pasodoble.

 Yo, personalmente pedí el dejar de hacer ese tipo de trabajo, precisamente un día que en Castellón me tocó estar en
 el callejón y me cabreé mucho al escuchar a un toro, al cual el torero falló cuatro veces con el estoque y harto de
 escuchar al pobre animal me quité los auriculares… No tuve bastante, que mientras agonizaba, escupía, se ahogaba en
 su sangre, se vino a morir justo pegado a mí, apoyado sobre las maderas mientras daba espasmos y su mirada
 ensangrentada y con lágrimas, sí lágrimas, sean o no sean de dolor, se cruzó con la mía y no nos la perdimos hasta
 que un inútil … falló dos veces con el descabello, al que le dije de todo.

 Ahí acabó mi temporada torera de por vida.

 Son sentimientos personales y lo mas probable es que a un amante de “la fiesta” le parezca ridículo, pero para mí,
 más ridículo es cuando después de semejante carnicería, giras la vista al público y los ves allí aplaudiendo,
 comiendo su bocata sin inmutarse, ni habiendo visto y oído lo que yo.
