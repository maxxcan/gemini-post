## mié 24 ago 2022 12:43:26 CEST Mi primer post


Este es el primero post que creo en esta cápsula. Aunque conozco Gemini desde las navidades pasadas, no me he atrevido a dar el salto a tener mi propia cápsula y empezar a añadir contenido. De hecho fue hace dos meses cuando por fin hice lo que más me costó que es organizar la cápsula y ver que enfoque le daba. Me plantee darle un enfoque más como una casa, o una estación espacial, y puede que en un futuro lo haga pero de momento su estructura se parece demasiado a un típico blog, con sus secciones y eso.

Pero como digo espero cambiar esto en un futuro y hacerlo algo más novedoso y original, o no, la verdad es que ahora lo importante es que tenga contenido, sino para que sirve una cápsula, blog o lo que sea.

Lo normal es que hable de qué es Gemini, de sus maravillas y cómo y por qué llegué hasta este universo. Lo haré pero de momento voy a llevar un rumbo un poco distinto. Más bien voy a empezar a explicar de donde estoy huyendo y cómo siento a Gemini y lo que se llama la smol web (sí así como suena se escribe en inglés) como un refugio en el que espero estar la mayor parte del tiempo.

Así que empezaremos con el problema y luego daremos la solución.

El principal problema del mundo tecnológico que a día de hoy está totalmente ligado con el mundo "real" y humano y nos afecta e influye todos los días. Hablamos por supuesto de Internet y como explicaremos en el siguiente post, *internet está rota*.


